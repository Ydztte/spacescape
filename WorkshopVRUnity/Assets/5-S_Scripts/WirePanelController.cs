﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WirePanelController : MonoBehaviour
{
    public WireScript[] wireNbr;
    public int wireInPosition = 0;
    public bool powerOn = false;

    void Start()
    {

    }

    void Update()
    {
        if (wireInPosition == wireNbr.Length)
        {
            powerOn = true;
        }

        if (powerOn == true)
        {
            foreach (WireScript wire in wireNbr)
            {
                Destroy(wire);
            }
        }
    }

    public void WireUpdate()
    {
        foreach (WireScript wire in wireNbr)
        {
            if (wire.inPosition == true && wireInPosition < wireNbr.Length)
            {
                wireInPosition += 1;
            }
            else if (wire.inPosition == false && wireInPosition > 0)
            {
                wireInPosition -= 1; // PUTAIN CA MARCHE PAS
            }
        }
    }
}
