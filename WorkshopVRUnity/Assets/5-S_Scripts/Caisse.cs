﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caisse : MonoBehaviour
{
    public GameObject couvercle;
    public GameObject loquet1;
    public GameObject loquet2;

    private bool active = false;
    
    void Update()
    {
        if (!loquet1.activeInHierarchy && !loquet2.activeInHierarchy && !active)
        {
            couvercle.AddComponent<Valve.VR.InteractionSystem.Throwable>();
            active = true;
        }
    }
}
