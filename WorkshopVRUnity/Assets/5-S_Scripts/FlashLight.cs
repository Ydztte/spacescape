﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLight : MonoBehaviour
{
    public Light flashLight;

    void Start()
    {
        flashLight = GetComponentInChildren<Light>();
        flashLight.enabled = false;
    }

    public void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "hand")
        {
            flashLight.enabled = !flashLight.enabled;
        }
    }
}