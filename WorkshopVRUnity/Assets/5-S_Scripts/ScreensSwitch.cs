﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreensSwitch : MonoBehaviour
{
    public GameObject noPower1;
    public GameObject noPower2;
    public GameObject powerOK;

    private Turbine turbine;
    private TableauElec tableauElec;

    public float t = 0;

    void Start()
    {
        turbine = GameObject.FindObjectOfType<Turbine>();
        tableauElec = GameObject.FindObjectOfType<TableauElec>();
    }

    void Update()
    {
        if (!turbine.globalTurbine && !tableauElec.powerActive)
        {
            t += Time.deltaTime;

            if (t >= 1.0f)
            {
                if (noPower1)
                {
                    noPower1.SetActive(false);
                    noPower2.SetActive(true);

                    t = 0;
                }

                if (noPower2)
                {
                    noPower1.SetActive(true);
                    noPower2.SetActive(false);

                    t = 0;
                }
            }
        }

        if (turbine.globalTurbine && tableauElec.powerActive)
        {
            noPower1.SetActive(false);
            noPower2.SetActive(false);
            powerOK.SetActive(true);
        }
    }
}
