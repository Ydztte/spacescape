﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Volant : MonoBehaviour
{
    public int index;

    public bool volantOK = false;

    public Transform trigger;

    private Valve.VR.InteractionSystem.CircularDrive circularDrive;
    private Rigidbody rb;

    private Valve.VR.InteractionSystem.Interactable interactable;
    private Valve.VR.InteractionSystem.Throwable throwable;
    private Valve.VR.InteractionSystem.VelocityEstimator velocityEstimator;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        circularDrive = GetComponentInChildren<Valve.VR.InteractionSystem.CircularDrive>();
        interactable = GetComponentInChildren<Valve.VR.InteractionSystem.Interactable>();
        throwable = GetComponentInChildren<Valve.VR.InteractionSystem.Throwable>();
        velocityEstimator = GetComponentInChildren<Valve.VR.InteractionSystem.VelocityEstimator>();

        circularDrive.enabled = false;
    }

    public void Update()
    {
        if (volantOK)
        {
            rb.useGravity = false;
            rb.isKinematic = true;
            circularDrive.enabled = true;
            transform.position = new Vector3(trigger.transform.position.x, trigger.transform.position.y, trigger.transform.position.z);
            gameObject.transform.eulerAngles = new Vector3(-90, 0, 0);
            //Destroy(interactable);
            Destroy(throwable);
            Destroy(velocityEstimator);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "VolantTrigger")
        {
            volantOK = true;
        }
    }
}
