﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(LoadAsyncOperation());
    }


    IEnumerator LoadAsyncOperation()
    {
        AsyncOperation gameScene = SceneManager.LoadSceneAsync(2);

        yield return new WaitForEndOfFrame();
    }
}
