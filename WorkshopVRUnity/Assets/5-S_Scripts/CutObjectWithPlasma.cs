﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutObjectWithPlasma : MonoBehaviour
{
    private Material material;

    public bool objectCut;

    private AudioSource cutSound;

    void Start()
    {
        material = GetComponent<Renderer>().material;

        cutSound = GetComponent<AudioSource>();
    }

    public void Update()
    {
        if (material.color == new Color(material.color.r, 0, 0))
        {
            objectCut = true;
            gameObject.SetActive(false);
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "CutterPlasma")
        {
            material.color = new Color(255, material.color.g - Time.deltaTime * 0.5f, material.color.b - Time.deltaTime * 0.5f);
            cutSound.Play();
        }
    }
}
