﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndexBouttonsElec : MonoBehaviour
{
    public int indexPowerButton;

    public bool buttonActive;

    private TableauElec tableauElec;


    public void Start()
    {
        tableauElec = GameObject.FindObjectOfType<TableauElec>();
    }

    public void OnMouseDown()
    {
        tableauElec.curPower += indexPowerButton;
    }

    public void PressFonction()
    {
        if (!buttonActive)
        {
            tableauElec.curPower += indexPowerButton;
        }
    }

    public void Update()
    {
        if (GetComponent<Valve.VR.InteractionSystem.HoverButton>().engaged == true)
        {
            buttonActive = true;
        }

        if (buttonActive)
        {
            transform.position = transform.position;
        }
    }
}
