﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fusible : MonoBehaviour
{
    public bool fusibleOK = false;

    public GameObject trigger;

    private Collider collide;
    private Rigidbody rb;
    private Valve.VR.InteractionSystem.Interactable interaction;
    private Valve.VR.InteractionSystem.Throwable throwable;

    private AudioSource plugSound;

    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        collide = GetComponent<CapsuleCollider>();
        interaction = GetComponent<Valve.VR.InteractionSystem.Interactable>();
        throwable = GetComponent<Valve.VR.InteractionSystem.Throwable>();
        plugSound = GetComponent<AudioSource>();
    }

    public void Update()
    {
        if (fusibleOK)
        {
            gameObject.transform.position = new Vector3(trigger.transform.position.x, trigger.transform.position.y, trigger.transform.position.z);
            gameObject.transform.eulerAngles = new Vector3(0, 0, 90);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "FusibleTrigger")
        {
            fusibleOK = true;
            SetFusiblePos(other.gameObject);
            Destroy(throwable);
            Destroy(interaction);
            plugSound.Play();
        }
    }

    public void SetFusiblePos (GameObject trigger)
    {
        rb.useGravity = false;
        rb.isKinematic = true;
        collide.isTrigger = true;
    }
}
