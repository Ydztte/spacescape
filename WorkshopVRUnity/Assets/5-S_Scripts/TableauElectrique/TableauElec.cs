﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableauElec : MonoBehaviour
{
    public bool powerActive = false;

    public GameObject interfaceElec;
    public GameObject jauge;

    public GameObject[] buttonsElec;

    public int curPower = 0;

    private Fusible fusible;
    private SimpleDoor[] simpleDoor;

    public AudioSource alarmSound;
    public AudioSource unlockSound;
    private bool hasPlayed = false;

    public GameObject spriteNoPower;
    public GameObject spritePower;

    void Start()
    {
        interfaceElec.SetActive(false);
        jauge.SetActive(false);

        fusible = GameObject.FindObjectOfType<Fusible>();
        simpleDoor = GameObject.FindObjectsOfType<SimpleDoor>();

        spriteNoPower.SetActive(true);
        spritePower.SetActive(false);
    }

    void Update()
    {
        if (fusible.fusibleOK == true)
        {
            interfaceElec.SetActive(true);
            jauge.SetActive(true);
        }

        jauge.transform.localScale = new Vector3(curPower * 0.01f, jauge.transform.localScale.y, jauge.transform.localScale.z);

        if (curPower == 10)
        {
            powerActive = true;
            alarmSound.Stop();

            for (int i = 0; i < simpleDoor.Length; i++)
            {
                if (simpleDoor[i].index == 2)
                {
                    simpleDoor[i].doorOpen = true;
                }
            }
            if (!hasPlayed)
            {
                unlockSound.Play();
                hasPlayed = true;
            }
        }
        else
        {
            powerActive = false;
        }

        if (curPower > 10)
        {
            curPower = 0;

            Reset();
        }

        if (powerActive)
        {
            spritePower.SetActive(true);
            spriteNoPower.SetActive(false);
        }
    }

    public void Reset()
    {
        for (int i = 0; i < buttonsElec.Length; i++)
        {
            buttonsElec[i].gameObject.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
        }
    }

}
