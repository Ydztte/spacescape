﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallLeaves : MonoBehaviour
{
    private float maxRotY = -40;
    private float minRotY = -15;

    private float maxRotZ = 20;
    private float minRotZ = -20;

    void Start()
    {
        transform.Rotate(new Vector3(transform.rotation.x, Random.Range(-40, -15), Random.Range(-180, 180)), Space.World);
    }
}
