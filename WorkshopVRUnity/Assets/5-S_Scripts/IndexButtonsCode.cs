﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndexButtonsCode : MonoBehaviour
{
    public int buttonIndex;

    public AccesCode parent;

    public void PressedButton()
    {
        parent = GameObject.FindObjectOfType<AccesCode>();
        gameObject.SetActive(false);
        parent.index += 1;
    }

    public void OnMouseDown()
    {
        parent = GameObject.FindObjectOfType<AccesCode>();
        gameObject.SetActive(false);
        parent.index += 1;
    }
}
