﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoEnd : MonoBehaviour
{
    public Animator anim;
    public GameObject button;

    public void GoAnim()
    {
        anim.SetBool("End", true);
        Destroy(button);
    }
}
