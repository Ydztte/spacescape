﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchPanelStart : MonoBehaviour
{
    public GameObject panelSpriteFlashlight1;
    public GameObject panelSpriteFlashlight2;

    public float timeSwitch;
    public float t;

    void Update()
    {
        t += Time.deltaTime;

        if (t >= timeSwitch)
        {
            if (panelSpriteFlashlight1.activeSelf)
            {
                panelSpriteFlashlight1.SetActive(false);
                panelSpriteFlashlight2.SetActive(true);

                t = 0;
            }
            else
            {
                panelSpriteFlashlight1.SetActive(true);
                panelSpriteFlashlight2.SetActive(false);

                t = 0;
            }
        }
    }
}
