﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetTurn : MonoBehaviour
{
    public Transform target;
    public float speedTurn;

    void Update()
    {
        transform.RotateAround(target.position, transform.up, speedTurn * Time.deltaTime);
    }
}
