﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireScript : MonoBehaviour
{
    public int wireCurrentValue;
    public int wireValue;
    public bool inPosition;
    public WirePanelController wirePanel;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OnMouseDown()
    {
        gameObject.transform.Rotate(90, 0, 0);
        wireCurrentValue += 1;

        if (wireCurrentValue > 3)
        {
            wireCurrentValue = 0;
        }

        if (wireCurrentValue == wireValue)
        {
            inPosition = true;
            wirePanel.WireUpdate();
            
        }
        else if (wireCurrentValue != wireValue)
        {
            inPosition = false;
            wirePanel.WireUpdate();
        }
    }

    public void PressFonction()
    {
        gameObject.transform.Rotate(90, 0, 0);
        wireCurrentValue += 1;

        if (wireCurrentValue > 3)
        {
            wireCurrentValue = 0;
        }

        if (wireCurrentValue == wireValue)
        {
            inPosition = true;
            wirePanel.WireUpdate();
        }
        else if (wireCurrentValue != wireValue)
        {
            inPosition = false;
            wirePanel.WireUpdate();
        }
    }
}
