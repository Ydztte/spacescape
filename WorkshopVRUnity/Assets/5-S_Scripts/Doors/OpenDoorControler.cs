﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoorControler : MonoBehaviour
{
    private Turbine turbine;
    private TableauElec tableauElec;
    public SimpleDoor simpleDoor;

    void Start()
    {
        turbine = GameObject.FindObjectOfType<Turbine>();
        tableauElec = GameObject.FindObjectOfType<TableauElec>();
    }

    void Update()
    {
        if (turbine.globalTurbine && tableauElec.powerActive)
        {
            simpleDoor.doorOpen = true;
        }
    }
}
