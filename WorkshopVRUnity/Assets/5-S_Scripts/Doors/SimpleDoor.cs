﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleDoor : MonoBehaviour
{
    public bool doorOpen;

    public int index;

    private Animator anim;

    private AudioSource doorSound;

    public GameObject lightRed;
    public GameObject lightGreen;

    public GameObject lightR;
    public GameObject lightG;

    private bool inStayTrigger = true;

    private void Start()
    {
        //Access components
        anim = GetComponent<Animator>();
        doorSound = GetComponent<AudioSource>();
        lightRed.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
        lightGreen.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");

        //Ini access door lights
        if (!doorOpen)
        {
            lightG.SetActive(false);
        }
        else
        {
            lightG.SetActive(true);
        }
    }

    
    public void OnTriggerStay(Collider other)
    {
        if (doorOpen && other.gameObject.tag == "Player" && inStayTrigger)
        {
            if (inStayTrigger)
            {
                doorSound.Play();
                inStayTrigger = false;
            }
             
            anim.SetBool("DoorOpen", true);

        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (doorOpen && other.gameObject.tag == "Player")
        {
            doorSound.Play();
            anim.SetBool("DoorOpen", true);
           
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (doorOpen && other.gameObject.tag == "Player")
        {
            doorSound.Play();
            anim.SetBool("DoorOpen", false);
        }
    }

    public void Update()
    {
        //Refresh access door lights
        if (!doorOpen)
        {
            lightRed.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
            lightGreen.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
            lightG.SetActive(false);
            lightR.SetActive(true);
        }
        else
        {
            lightRed.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
            lightGreen.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
            lightG.SetActive(true);
            lightR.SetActive(false);
        }
    }
}
