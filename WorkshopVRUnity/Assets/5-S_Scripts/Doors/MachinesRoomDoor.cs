﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachinesRoomDoor : MonoBehaviour
{
    private WirePanelController wires;
    public SimpleDoor simpleDoor;

    void Start()
    {
        wires = GameObject.FindObjectOfType<WirePanelController>();
    }

    void Update()
    {
        if (wires.powerOn)
        {
            simpleDoor.doorOpen = true;
        }
    }
}
