﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turbine : MonoBehaviour
{
    public bool turbine1 = false;
    public bool turbine2 = false;

    public bool globalTurbine = false;

    public AudioSource turbineSound;
    private bool startedLoop;


    public void Update()
    {
        if (turbine1 && turbine2)
        {
            globalTurbine = true;
            
        }
        if (globalTurbine == true)
        {
            if (!startedLoop)
            {
                turbineSound.Play();
                startedLoop = true;
            }
        }
    }
}
