﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Générateur2 : MonoBehaviour
{
    private Turbine turbine;

    public GameObject obstacle;

    public bool obstacleOK;

    public GameObject spriteObstruction;
    public GameObject spriteTurbineOK;
    public GameObject GOEtincelle;

    void Start()
    {
        turbine = GameObject.FindObjectOfType<Turbine>();

        spriteObstruction.SetActive(true);
        spriteTurbineOK.SetActive(false);
        GOEtincelle.SetActive(true);
    }

    public void ButtonPressed()
    {
        if (obstacleOK)
        {
            turbine.turbine2 = true;
            spriteTurbineOK.SetActive(true);
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Obstacle")
        {
            obstacleOK = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Obstacle")
        {
            obstacleOK = true;
        }
    }

    public void ObstacleDelete()
    {
        obstacleOK = true;
        spriteObstruction.SetActive(false);
        GOEtincelle.SetActive(false);
    }
}
