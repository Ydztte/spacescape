﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Générateur1 : MonoBehaviour
{
    public GameObject buttonDrop;
    public GameObject button;

    private Turbine turbine;

    public GameObject spriteTurbinOK;

    public void Start()
    {
        turbine = GameObject.FindObjectOfType<Turbine>();
        spriteTurbinOK.SetActive(false);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "BoutonDrop")
        {
            Destroy(buttonDrop);
            button.SetActive(true);
        }
    }

    public void PressBouton()
    {
        turbine.turbine1 = true;
        spriteTurbinOK.SetActive(true);
    }
}
