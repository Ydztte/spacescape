﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSimpleDoor : MonoBehaviour
{
    public int buttonIndex;

    public SimpleDoor[] simpleDoor;

    private bool buttonActive;

    void Start()
    {
        simpleDoor = GameObject.FindObjectsOfType<SimpleDoor>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            for (int i = 0; i < simpleDoor.Length; i++)
            {
                if (buttonIndex == simpleDoor[i].index)
                {
                    simpleDoor[i].doorOpen = true;
                }
            }
        }
    }
}
