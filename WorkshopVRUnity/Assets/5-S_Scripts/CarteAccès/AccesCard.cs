﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccesCard : MonoBehaviour
{
    public int index;

    public GameObject lightRed;
    public GameObject lightGreen;

    public GameObject lightR;
    public GameObject lightG;

    public bool accesAccept = false;

    private SimpleDoor[] door;

    private AudioSource unlockSound;

    public void Start()
    {
        door = GameObject.FindObjectsOfType<SimpleDoor>();
        unlockSound = GetComponent<AudioSource>();

        lightG.SetActive(false);
        lightR.SetActive(false);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "AccesCard")
        {
            accesAccept = true;

            for (int i = 0; i < door.Length; i++)
            {
                if (door[i].GetComponent<SimpleDoor>().index == index)
                {
                    door[i].GetComponent<SimpleDoor>().doorOpen = true;
                }
            }
            unlockSound.Play();
        }
    }

    public void Update()
    {
        if (!accesAccept)
        {
            lightRed.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
            lightGreen.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
            lightG.SetActive(false);
            lightR.SetActive(true);
        }
        else
        {
            lightRed.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
            lightGreen.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
            lightG.SetActive(true);
            lightR.SetActive(false);
        }
    }
}
